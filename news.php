<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title">News</h2>
                            <?php
                            if($num_news < 1)
                            {
                                echo '
                                <div class="alert alert-warning" role="alert">
                                    <i class="fad fa-exclamation-circle"></i> There are no news
                                </div>
                                ';
                            } else {
                                while ($res_news = $news_query->fetch_assoc())
                                {
                                    $NewsID = $res_news['id'];
                                    $NewsHead = $res_news['header'];
                                    $NewsBody = $res_news['news'];
                                    $NewsAuthor = $res_news['author'];
                                    $NewsTime = $res_news['created_at'];

                                    echo '
                                    <div class="card bg-transparent" style="margin-bottom: 10px;">
                                    <div class="show" aria-labelledby="headingOne">
                                    <div class="card-body">
                                    <h4>'.$NewsHead.'</h4>
                                    <hr />
                                    <p>'.$NewsBody.'</p>
                                    <hr />
                                    <p><span style="width: 50%; text-align: left; float: left;">Author: '.$NewsAuthor.'</span><span style="width: 50%; text-align: right; float: left;"> Time: '.$NewsTime.'</span></p>
                                    </div>
                                    </div>
                                    </div>
                                    ';
                                }
                            }
                            ?>
                    </div>
                    <div class="card-footer text-muted text-info">
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>