<?php include('header.php'); ?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-gift"></i> Shop</h2>

                        <table class="table table-hover" id="shopTable">
                        <input class="form-control" type="text" id="shopSearch" onkeyup="shopSearch()" placeholder="Search">
                            <thead>
                            <tr>
                                <th scope="col">Item name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Category</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //let's get the items from the store
                            $item_query = $mysqliA->query("SELECT * FROM `store_items`;") or die (mysqli_error($mysqliA));
                            $num_items = $item_query->num_rows;
                            if($num_items < 1)
                            {
                                echo '
                                    <div class="alert alert-warning" role="alert">
                                      <i class="fad fa-exclamation-circle"></i> There are no items for sale!
                                    </div>
                                ';
                            }
                            else
                            {
                                while($res_item = $item_query->fetch_assoc())
                                {
                                    $ItemID = $res_item['item_id'];
                                    $product_name = $res_item['item_name'];
                                    $ProductID = $res_item['id'];
                                    $itemcatid = $res_item['category'];
                                    $pricedb = $res_item['price'];

                                    // let´s query store category by itemcatid
                                    $category_query = $mysqliA->query("SELECT * FROM `store_items_categorys` WHERE id = $itemcatid;") or die (mysqli_error($mysqliA));
                                    while($res_category = $category_query->fetch_assoc())
                                    {
                                        $category = $res_category['name'];
                                    }

                                    if($pricedb == 0)
                                    {
                                        $price = "Free";
                                    }

                                    if($pricedb > 0)
                                    {
                                        $price = $pricedb;
                                    }

                                    echo '
                                    <tr>
                                        <td><a href="#" data-wowhead="item='. $ItemID .'">'. $product_name .'</a></td>
                                        <td><span class="badge badge-warning">'. $price .' <i class="fad fa-coin"></i></span></td>
                                        <td><span>'. $category .'</span></td>
                                        <td><a href="'.$custdir.'/product.php?id='. $ProductID .'" class="badge badge-info"><i class="fad fa-cart-plus"></i> Purchase</a>
                                        </td>
                                    </tr>
                                    ';
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>