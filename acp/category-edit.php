<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-file-edit"></i> Category edit</div>
            <div class="card-body">
                <?php
                if(isset($_POST['editcat']))
                {
                    $catid = stripslashes(mysqli_real_escape_string($mysqliA, $_GET['id']));
                    $catname = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['catname']));

                    if(empty($catid && $catname))
                    {
                        echo '
                            <div class="alert alert-warning" role="alert">
                              <i class="fad fa-exclamation-triangle"></i> Invalid Category ID!
                            </div>
                         ';
                        header("refresh:3; url=$custdir/acp/store-categorys.php");
                    }
                    else
                    {
                        //insert
                        $cat_update = $mysqliA->query("UPDATE `store_items_categorys` SET `name` = '$catname' WHERE `id` = '$catid';") or die (mysqli_error($mysqliA));
                        if($cat_update === true)
                        {
                            echo '
                                <div class="alert alert-success" role="alert">
                                  <i class="fad fa-check-circle"></i> Categorys was edited
                                </div>
                            ';
                            header("refresh:3; url=$custdir/acp/store-categorys.php");
                        }
                    }
                }
                else
                {
                    //get data from category already
                    $catid = stripslashes(mysqli_real_escape_string($mysqliA, $_GET['id']));
                    if(empty($catid))
                    {
                        echo '
                            <div class="alert alert-warning" role="alert">
                              <i class="fad fa-exclamation-triangle"></i> Invalid category ID 1
                            </div>
                        ';
                    }
                    else
                    {
                        //get data from store
                        $get_cat = $mysqliA->query("SELECT * FROM `store_items_categorys` WHERE `id` = '$catid';") or die (mysqli_error($mysqliA));
                        $num_res = $get_cat->num_rows;
                        if($num_res < 1)
                        {
                            echo '
                                <div class="alert alert-warning" role="alert">
                                  <i class="fad fa-exclamation-triangle"></i> Invalid category ID 2
                                </div>
                            ';
                        }
                        else
                        {
                            while($res = $get_cat->fetch_assoc())
                            {
                                $cat_Name = $res['name'];
                            echo '
                            <form name="editcat" method="post">
                                <div class="form-group">
                                    <label for="catname">Category Name</label>
                                    <input type="text" name="catname" class="form-control" value="'. $cat_Name .'" required="required">
                                    <small>Name it what ever you want</small>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" name="editcat"><i class="fad fa-file-edit"></i> Edit item</button>
                                </div>
                            </form>
                            ';
                            }
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>