<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>
        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-globe-americas"></i> Site Settings</div>
            <div class="card-body">
                <?php
                //papyal thingy
                if(isset($_POST['site_update']))
                {
                    $site_name = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['site_name']));
                    $site_description = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['site_description']));
                    $site_contact = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['site_contact']));
                    $site_discord = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['site_discord']));
                    //update
                    $paypal_update = $mysqliA->query("UPDATE `site_settings` SET `site_name` = '$site_name', `site_description` = '$site_description', `site_contact` = '$site_contact', `site_discord` = '$site_discord'")  or die (mysqli_error($mysqliA));
                    if($paypal_update === true)
                    {
                        echo '
                            <div class="alert alert-success" role="alert">
                                <i class="fad fa-spinner-third fa-spin"></i> Your settings is updating. Please wait...!
                            </div>
                            ';
                        header("refresh:3; url=$custdir/acp/site-settings.php");
                    }
                    else
                    {
                        echo '
                            <div class="alert alert-warning" role="alert">
                                <i class="fad fa-exclamation-triangle"></i> There\'s been an error! Please try again!<br />If this error continues please contact us on discord!
                            </div>
                                ';
                        header("refresh:5; url=$custidr/acp/site-settings.php");
                    }
                }
                else
                {
                    //let's get data from db
                    $site_query = $mysqliA->query("SELECT * FROM `site_settings`") or die (mysqli_error($mysqliA));
                    while($site_ress = $site_query->fetch_assoc())
                    {
                        $site_name = $site_ress['site_name'];
                        $site_description = $site_ress['site_description'];
                        $site_contact = $site_ress['site_contact'];
                        $site_discord = $site_ress['site_discord'];
                    }
                    echo '
                        <form name="paypal_update" method="post" action="">
                            <div class="form-group">
                                <label for="site_name"></label>
                                <input type="text" name="site_name" class="form-control" value="' .  $site_name. '" required>
                                <small>Enter here your desired website title</small>
                            </div>
                            <div class="form-group">
                                <label for="site_description">Website Description</label>
                                <input type="text" name="site_description" class="form-control" value="' .  $site_description. '" required>
                                <small>Enter here your desired website descrpiton</small>
                            </div>  
                            <div class="form-group">
                                <label for="site_contact">Contact Email address</label>
                                <input type="text" name="site_contact" class="form-control" value="' .  $site_contact. '" required>
                                <small>Enter here your contact email address for the website</small>
                            </div>  
                            <div class="form-group">
                                <label for="site_discord">Discord Invite Link</label>
                                <input type="text" name="site_discord" class="form-control" value="' .  $site_discord. '" required>
                                <small>Enter here your discord invite link!</small>
                            </div>  
                            <button type="submit" name="site_update" class="btn btn-primary"><i class="fad fa-check-circle"></i> Update this settings!</button>
                        </form>
                        ';
                }
                ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>