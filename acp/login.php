<?php
include('../config.php');
if($site_dir == '') {
    $custdir = '';
} elseif ($site_dir != '') 
{ 
    $custdir = '/'.$site_dir;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CoreCms Admin Manager">
    <meta name="author" content="laur">

    <title>CoreCms Manager - Login</title>

    <!-- Bootstrap core CSS-->
    <link href="<?php echo $custdir; ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="<?php echo $custdir; ?>/assets/fontawesome/css/all.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo $custdir; ?>/acp/css/sb-admin.css" rel="stylesheet">
	<script src='https://www.google.com/recaptcha/api.js' async defer></script>

</head>

<body class="bg-dark">

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login to <?php echo $site_name; ?> Manager</div>
        <div class="card-body">
            <?php
            if(isset($_SESSION['acp']))
            {
                echo 'You are already logged in';
                header("Location: $custdir/acp/dashboard.php");
            }
            else
            {
                if(isset($_POST['login']))
                {
                    //to be completed :P
                    //do insert
                    $password = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['password']));
                    $email = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['email']));

                    $bnet_pass = bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($email)).":".strtoupper($password)))))));
                    $bnet_upper_pass = strtoupper($bnet_pass);

                    //check user
                    //let's check login
                    $check_login = $mysqliA->query("SELECT * FROM `battlenet_accounts` WHERE `email` = '$email' AND `sha_pass_hash` = '$bnet_upper_pass';") or die (mysqli_error($mysqliA));
                    $num_acc = $check_login->num_rows;
                    if($num_acc < 1)
                    {
                        echo '
                                        <div class="alert alert-warning" role="alert">
                                            <i class="fad fa-exclamation-circle"></i> There is no user with this email address!
                                        </div>
                                    ';
                        header("refresh:3; url=$custdir/acp/login.php");
                    }
                    else
                    {
                        while($login = $check_login->fetch_assoc())
                        {
                            $session_id = $login['id'];
                            //let's check for rank :D
                            $user_rank = $mysqliA->query("SELECT * FROM `account_access` WHERE `id` = '$session_id';") or die (mysqli_error($mysqliA));
                            $count_rows = $user_rank->num_rows;
                            if($count_rows < 1)
                            {
                                echo '
                                <div class="alert alert-warning" role="alert">
                                    <i class="fad fa-exclamation-triangle"></i> You don\'t have enough privileges to access this page!
                                </div>
                            ';
                                header("refresh:5; url=$custdir/acp");
                            }
                            else
                            {
                                $_SESSION['acp'] = $session_id;
                                echo '
                                <div class="alert alert-success" role="alert">
                                    <i class="fad fa-check"></i> You are now logged in!
                                </div>
                            ';
                                header("refresh:3; url=$custdir/acp/dashboard.php");
                            }
                        }
                    }

                }
                else {
                    ?>
                    <form name="login" action="" method="post">
                        <div class="form-group">
                            <div class="form-label-group">
                                <input name="email" type="text" id="inputEmail" class="form-control" required="required" autofocus="autofocus">
                                <label for="inputEmail">Email address</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <input name="password" type="password" id="inputPassword" class="form-control"required="required">
                                <label for="inputPassword">Password</label>
                            </div>
                        </div>
                        <button type="submit" name="login" class="btn btn-primary form-control">Login <i class="fal fa-sign-in"></i></button>
                    </form>
					<div class="text-center">
						Best use with <a href="https://www.mozilla.org/en-US/firefox/download/thanks/" target="_blank">Mozilla Firefox</a>
						<br />
						<br />
						<a class="d-block small" href="#">Forgot passowrd?</a>
					</div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo $custdir; ?>/assets/jquery/jquery.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo $custdir; ?>/assets/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
