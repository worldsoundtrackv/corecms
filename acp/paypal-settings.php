<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
                </li>
            </ol>
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fab fa-paypal"></i> PayPal Settings</div>
                <div class="card-body">
                    <?php
                    //papyal thingy
                    if(isset($_POST['paypal_update']))
                    {
                        $paypal_client_id = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['paypal_client_id']));
                        $paypal_client_secret = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['paypal_client_secret']));
                        $paypal_return_url = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['paypal_return_url']));
                        $paypal_cancel_url = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['paypal_cancel_url']));
                        $paypal_currency = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['paypal_currency']));
                        $paypal_status = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['paypal_status']));
                        //update
                        $paypal_update = $mysqliA->query("UPDATE `site_settings` SET `paypal_client_id` = '$paypal_client_id', `paypal_client_secret` = '$paypal_client_secret', `paypal_return_url` = '$paypal_return_url', `paypal_cancel_url` = '$paypal_cancel_url', `paypal_currency` = '$paypal_currency', `paypal_status` = '$paypal_status';")  or die (mysqli_error($mysqliA));
                        if($paypal_update === true)
                        {
                            echo '
                            <div class="alert alert-success" role="alert">
                                <i class="fad fa-spinner-third fa-spin"></i> Your settings is updating. Please wait...!
                            </div>
                            ';
                            header("refresh:3; url=$custdir/acp/paypal-settings.php");
                        }
                        else
                        {
                            echo '
                            <div class="alert alert-warning" role="alert">
                                <i class="fad fa-exclamation-triangle"></i> There\'s been an error! Please try again!<br />If this error continues please contact us on discord!
                            </div>
                                ';
                            header("refresh:5; url=$custdir/acp/paypal-settings.php");
                        }
                    }
                    else
                    {
                        //let's get data from db
                        $paypal_query = $mysqliA->query("SELECT * FROM `site_settings`") or die (mysqli_error($mysqliA));
                        while($pp_ress = $paypal_query->fetch_assoc())
                        {
                            $paypal_client_id = $pp_ress['paypal_client_id'];
                            $paypal_client_secret = $pp_ress['paypal_client_secret'];
                            $paypal_return_url = $pp_ress['paypal_return_url'];
                            $paypal_cancel_url = $pp_ress['paypal_cancel_url'];
                            $paypal_currency = $pp_ress['paypal_currency'];
                            $paypal_status = $pp_ress['paypal_status'];
                        }
                        echo '
                        <div class="alert alert-info" role="alert">
                            <i class="fad fa-exclamation-circle"></i> Please go to <strong><a href="https://developer.paypal.com/developer/applications/create" target="_blank">PayPal Developer Portal</a></strong> (login with your paypal account, if you don\'t have one create a new one) and create a new <strong class="text-success">Live APP</strong> not <strong class="text-danger">Sandbox APP</strong> and then complete the form below!
                        </div>
                        <form name="paypal_update" method="post" action="">
                            <div class="form-group">
                                <label for="paypal_client_id">PayPal Client ID</label>
                                <input type="text" name="paypal_client_id" class="form-control" value="' .  $paypal_client_id. '" required>
                                <small>Client ID should look like "ATXzCdOsj0Qo6-gq4TyLtfVuv5RNsWASDSpXHpXP1teNz-l5RIfP2ijqPazyFeCx6c6QdH35rFSOZNVPN"</small>
                            </div>
                            <div class="form-group">
                                <label for="paypal_client_id">PayPal Client Secret Key</label>
                                <input type="text" name="paypal_client_secret" class="form-control" value="' .  $paypal_client_secret. '" required>
                                <small>Client Secret Key should look like "ED6JcvQlXoNWZVVS8IiR-WASdE4CHxssTFJqZLM58uJrYzBu1Waj5fdw7smUxtPK30Pmhbv-ze4hWGwB"</small>
                            </div>  
                            <div class="form-group">
                                <label for="paypal_client_id">PayPal Return URL</label>
                                <input type="text" name="paypal_return_url" class="form-control" value="' .  $paypal_return_url. '" required>
                                <small>This should be set to your domain name and "success.php" file (ex: http://www.myserver.com/succcess.php)</small>
                            </div>  
                            <div class="form-group">
                                <label for="paypal_client_id">PayPal Cancel URL</label>
                                <input type="text" name="paypal_cancel_url" class="form-control" value="' .  $paypal_cancel_url. '" required>
                                <small>This should be set to your domain name and "cancel.php" file (ex: http://www.myserver.com/cancel.php)</small>
                            </div>  
                            <div class="form-group">
                                <label for="paypal_client_id">PayPal Currency</label>
                                <select name="paypal_currency" class="form-control" required>
                                    <option value="' .  $paypal_currency. ' ">' .  $paypal_currency. ' - Current setting</option>
                                    <option disabled>--</option>
                                    <option value="USD">USD</option>
                                    <option value="EUR">EUR</option>
                                </select>
                                <small>Use your desired currency EUR or USD (contact us if you want a different currency but check it first if paypal supports your desired currency)</small>
                            </div>
                            <div class="form-group">
                                <label for="paypal_client_id">PayPal Test Mode</label>
                                <select name="paypal_status" class="form-control" required>
                                    <option value="' .  $paypal_status. ' ">' . strtoupper($paypal_status) . ' - Current setting</option>
                                    <option disabled>--</option>
                                    <option value="false">FALSE</option>
                                    <option value="true">TRUE</option>
                                </select>
                                <small>Set to false to go live with PayPal payments</small>
                            </div>
                            <button type="submit" name="paypal_update" class="btn btn-primary"><i class="fad fa-check-circle"></i> Update this settings!</button>
                        </form>
                        <br />
                        <div class="alert alert-warning" role="alert">
                            <i class="fad fa-exclamation-circle"></i> Please note! If you don\'t set this right your <span class="badge badge-warning"><i class="fad fa-coin"></i></span> (coins) store won\'t be working! Contact us on discord if you need help!
                        </div>
                        ';
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
<?php
include ('footer.php');
?>